import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { withThemeProvider } from './store/Store';
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import Box from '@material-ui/core/Box';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Switch from '@material-ui/core/Switch';
import { useStore } from './store/Store';

const useStyles = makeStyles(theme => ({
  root: {
    width: '100%',
    height: '100%',
  },
}));

const App: React.FC = () => {
  const classes = useStyles();
  const { darkMode, setDarkMode } = useStore();
  return (
    <Grid
      className={classes.root}
      container
      justify="center"
      alignItems="center"
    >
      <Paper>
        <Box p={10}>
          <FormControlLabel
            control={
              <Switch
                checked={darkMode}
                onChange={() => setDarkMode(!darkMode)}
              />
            }
            label="Dark Mode"
          />
        </Box>
      </Paper>
    </Grid>
  );
};

export default withThemeProvider(App);
